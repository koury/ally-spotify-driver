# Ally Spotify Driver

A [AdonisJS v5+ Ally](https://github.com/adonisjs/ally) driver for [Spotify](https://www.spotify.com).

## Setup

```bash
npm i ally-spotify-driver

node ace configure ally-spotify-driver
```

## Contributing

There are multiple ways to contribute to this project, you can:

- Create new issue
- Improve existing issues issues by adding comments
- Submit a pull request
