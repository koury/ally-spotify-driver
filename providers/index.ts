import type { ApplicationContract } from '@ioc:Adonis/Core/Application'

export default class SpotifyDriverProvider {
  constructor(protected app: ApplicationContract) {}

  public async boot() {
    const Ally = this.app.container.resolveBinding('Adonis/Addons/Ally')
    const { SpotifyDriver } = await import('../src/SpotifyDriver')

    Ally.extend('spotify', (_, __, config, ctx) => {
      return new SpotifyDriver(ctx, config)
    })
  }
}
