The package has been configured successfully!

Make sure to first define the mapping inside the `contracts/ally.ts` file as follows.

```ts
import { SpotifyDriverConfig, SpotifyDriver } from 'ally-spotify-driver/build/standalone'

declare module '@ioc:Adonis/Addons/Ally' {
  interface SocialProviders {
    spotify: {
      config: SpotifyDriverConfig
      implementation: SpotifyDriver
    }
  }
}
```
