/*
|--------------------------------------------------------------------------
| Ally Oauth driver for Spotify
|--------------------------------------------------------------------------
*/

import type { AllyUserContract } from '@ioc:Adonis/Addons/Ally'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { Oauth2Driver, ApiRequest, RedirectRequest } from '@adonisjs/ally/build/standalone'

/**
 * Shape of the Spotify access token
 */
export type SpotifyAccessToken = {
  token: string
  type: 'bearer'
  scope: string
}

/**
 * Available Spotify scopes
 * https://developer.spotify.com/documentation/general/guides/scopes/#scopes
 */
export type SpotifyScopes =
  // Images
  | 'ugc-image-upload'
  // Listening History
  | 'user-read-recently-played'
  | 'user-top-read'
  | 'user-read-playback-position'
  // Spotify Connect
  | 'user-read-playback-state'
  | 'user-modify-playback-state'
  | 'user-read-currently-playing'
  // Playback
  | 'app-remote-control'
  | 'streaming'
  // Playlists
  | 'playlist-modify-public'
  | 'playlist-modify-private'
  | 'playlist-read-private'
  | 'playlist-read-collaborative'
  // Follow
  | 'user-follow-modify'
  | 'user-follow-read'
  // Library
  | 'user-library-modify'
  | 'user-library-read'
  // Users
  | 'user-read-email'
  | 'user-read-private'

/**
 * All options available for Spotify
 */
export type SpotifyDriverConfig = {
  driver: 'spotify'
  clientId: string
  clientSecret: string
  callbackUrl: string
  scopes?: Array<SpotifyScopes>
  userInfoUrl?: string
}

/**
 * Driver implementation
 */
export class SpotifyDriver extends Oauth2Driver<SpotifyAccessToken, SpotifyScopes> {
  /**
   * The URL for the redirect request. The user will be redirected on this page
   * to authorize the request.
   *
   * Do not define query strings in this URL.
   */
  protected authorizeUrl = 'https://accounts.spotify.com/authorize'

  /**
   * The URL to hit to exchange the authorization code for the access token
   *
   * Do not define query strings in this URL.
   */
  protected accessTokenUrl = 'https://accounts.spotify.com/api/token'

  /**
   * The URL to hit to get the user details
   *
   * Do not define query strings in this URL.
   */
  protected userInfoUrl = 'https://api.spotify.com/v1/me'

  /**
   * The param name for the authorization code.
   */
  protected codeParamName = 'code'

  /**
   * The param name for the error.
   */
  protected errorParamName = 'error'

  /**
   * Cookie name for storing the CSRF token.
   */
  protected stateCookieName = 'spotify_oauth_state'

  /**
   * Parameter name to be used for sending and receiving the state from.
   */
  protected stateParamName = 'state'

  /**
   * Parameter name for sending the scopes to the oauth provider.
   */
  protected scopeParamName = 'scope'

  /**
   * The separator indentifier for defining multiple scopes
   */
  protected scopesSeparator = ' '

  constructor(ctx: HttpContextContract, public config: SpotifyDriverConfig) {
    super(ctx, config)

    this.loadState()
  }

  /**
   * Optionally configure the authorization redirect request. The actual request
   * is made by the base implementation of "Oauth2" driver and this is a
   * hook to pre-configure the request.
   */
  protected configureRedirectRequest(request: RedirectRequest<SpotifyScopes>) {
    /**
     * Define user defined scopes or the default one's
     */
    request.scopes(this.config.scopes || ['user-read-email'])

    /**
     * Set "response_type" param
     */
    request.param('response_type', 'code')
  }

  /**
   * Optionally configure the access token request. The actual request is made by
   * the base implementation of "Oauth2" driver and this is a hook to pre-configure
   * the request
   */
  // protected configureAccessTokenRequest(request: ApiRequest) {}

  /**
   * Update the implementation to tell if the error received during redirect
   * means "ACCESS DENIED".
   */
  public accessDenied() {
    return this.ctx.request.input('error') === 'user_denied'
  }

  /**
   * Get the user details by query the provider API. This method must return
   * the access token and the user details both. Checkout the google
   * implementation for same.
   *
   * https://github.com/adonisjs/ally/blob/develop/src/Drivers/Google/index.ts#L191-L199
   */
  public async user(
    callback?: (request: ApiRequest) => void
  ): Promise<AllyUserContract<SpotifyAccessToken>> {
    const accessToken = await this.accessToken()
    const request = this.httpClient(this.config.userInfoUrl || this.userInfoUrl)
    const user = await this.getUserInfo(accessToken.token, callback)

    /**
     * Allow end user to configure the request. This should be called after your custom
     * configuration, so that the user can override them (if required)
     */
    if (typeof callback === 'function') {
      callback(request)
    }

    return {
      ...user,
      token: accessToken,
    }
  }

  /**
   * Fetches the user info using the Spotify API
   */
  protected async getUserInfo(token: string, callback?: (request: ApiRequest) => void) {
    const request = this.getAuthenticatedRequest(this.config.userInfoUrl || this.userInfoUrl, token)
    if (typeof callback === 'function') {
      callback(request)
    }

    const body = await request.get()

    return {
      id: body.id,
      name: body.display_name,
      nickName: body.display_name,
      avatarUrl: body.images[0]?.url,
      email: body.email || null,
      emailVerificationState: 'unsupported' as const,
      original: body,
    }
  }

  /**
   * Returns the HTTP request with the authorization header set
   */
  protected getAuthenticatedRequest(url: string, token: string) {
    const request = this.httpClient(url)
    request.header('Authorization', `Bearer ${token}`)
    request.header('Accept', 'application/json')
    request.parseAs('json')
    return request
  }

  public async userFromToken(
    token: string,
    callback?: (request: ApiRequest) => void
  ): Promise<AllyUserContract<{ token: string; type: 'bearer' }>> {
    const request = this.httpClient(this.config.userInfoUrl || this.userInfoUrl)
    const user = await this.getUserInfo(token, callback)

    /**
     * Allow end user to configure the request. This should be called after your custom
     * configuration, so that the user can override them (if required)
     */
    if (typeof callback === 'function') {
      callback(request)
    }

    return {
      ...user,
      token: { token, type: 'bearer' as const },
    }
  }
}
